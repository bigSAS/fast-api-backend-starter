# Fast API backend starter

## Usage

### With docker-compose
To run the application you need to have `Docker` and `docker-compose` installed. So, just execute from the root directory:

```bash
docker-compose up
```

#### Swagger  

```/docs```  

#### OpenAPI docs

```/redoc```  

So, run the first migration:

```bash
docker-compose exec app alembic upgrade head
```

Create migration:

```bash
docker-compose exec app alembic revision --autogenerate -m "{message}"
docker-compose exec app alembic upgrade head
```

### Tests
**To run the tests:**

```
docker-compose exec app pytest <args>
```

**To re-run the tests**, firstly, we recreate the database because there are unit tests which create resources, so if it already exists the test will fail:

Remove the data files before recreate the container
```
rm -fr db_data/*
```
Recreate the db service:

```docker
docker-compose stop db
docker-compose rm db
docker-compose up -d db
```

Finally, re-run the migration and the tests:
```
docker-compose exec app alembic upgrade head
docker-compose exec app pytest
```
